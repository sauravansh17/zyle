import 'package:ZYLE/screens/by_chat_room.dart';
import 'package:ZYLE/screens/by_map_room.dart';
import 'package:flutter/material.dart';
import 'package:flutter_speed_dial/flutter_speed_dial.dart';

class CustomFloatingBtn extends StatefulWidget {
  @override
  _CustomFloatingBtnState createState() => _CustomFloatingBtnState();
}

class _CustomFloatingBtnState extends State<CustomFloatingBtn> {
  bool _dialVisible = true;

  @override
  Widget build(BuildContext context) {
    return SpeedDial(
      marginRight: 18,
      marginBottom: 20,
      child: Icon(Icons.add),
      animatedIcon: AnimatedIcons.menu_close,
      animatedIconTheme: IconThemeData(size: 22.0),
      visible: _dialVisible,
      closeManually: false,
      curve: Curves.bounceIn,
      overlayColor: Colors.black,
      overlayOpacity: 0.5,
      onOpen: () => print('OPENING DIAL'),
      onClose: () => print('DIAL CLOSED'),
      tooltip: 'Find',
      heroTag: 'speed-dial-hero-tag',
      backgroundColor: Color(0xffb29fff),
      foregroundColor: Colors.black,
      elevation: 8.0,
      shape: CircleBorder(),
      children: [
        SpeedDialChild(
            child: Icon(Icons.map),
            backgroundColor: Colors.green,
            label: 'By Map',
            labelStyle: TextStyle(fontSize: 18.0),
            onTap: () => {
                  Navigator.push(context,
                      MaterialPageRoute(builder: (context) => ByMapRoom()))
                }),
        SpeedDialChild(
          child: Icon(Icons.chat),
          backgroundColor: Colors.blue,
          label: 'By Chat',
          labelStyle: TextStyle(fontSize: 18.0),
          onTap: () => {
            Navigator.push(
                context, MaterialPageRoute(builder: (context) => ByChatRoom()))
          },
        ),
      ],
    );
  }
}
