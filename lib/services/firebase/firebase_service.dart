import 'package:cloud_firestore/cloud_firestore.dart';

class FirebaseService {
  static FirebaseFirestore firestoreInstance = FirebaseFirestore.instance;

  static Future<bool> storeMsg(senderId, receiverId, msg) async {
    try {
      var value = await firestoreInstance
          .collection('messages')
          .doc(senderId)
          .collection(receiverId)
          .add({
        'senderId': senderId,
        'receiverId': receiverId,
        'text': msg,
        'read': false,
        'timestamp': new DateTime.now()
      });
      print("msg stored");
      return true;
    } catch (e) {
      print("failed to store message");
      print(e);
      return false;
    }
  }

  static Future<bool> sendMsg(senderId, receiverId, msg) async {
    try {
      var value = await firestoreInstance
          .collection('messages')
          .doc(receiverId)
          .collection(senderId)
          .add({
        'senderId': senderId,
        'receiverId': receiverId,
        'text': msg,
        'read': false,
        'timestamp': new DateTime.now()
      });
      print("msg sent");
      return true;
    } catch (e) {
      print(e);
      print("failed to store message");
      return false;
    }
  }

  static Future<bool> updateChatlist(senderId, receiverId) async {
    try {
      var value = await firestoreInstance
          .collection('chatlist')
          .doc(receiverId)
          .collection(receiverId)
          .add({
        'senderId': senderId,
        'name': 'name',
        'photo': 'photo',
        'last_msg': '',
        'last_msg_timestamp': 'timestamp'
      });
      // update({'chatlist':[receiverId]});
      print("added to chatlist");
      return true;
    } catch (e) {
      print(e);
      print("failed to add to chatlist");
      return false;
    }
  }

  static Stream<QuerySnapshot> getChatlist(senderId) {
    return firestoreInstance
        .collection("chatlist")
        .doc(senderId)
        .collection(senderId)
        .snapshots();
  }

  static Stream<QuerySnapshot> getMsglist(senderId, receiverId) {
    return firestoreInstance
        .collection('messages')
        .doc(senderId)
        .collection(receiverId)
        .orderBy("timestamp", descending: true)
        .snapshots();
  }
}
