import 'package:shared_preferences/shared_preferences.dart';

class AppStorage {
  Future<void> set(key, value) async {
    SharedPreferences _prefs = await SharedPreferences.getInstance();
    _prefs.setString(key, value);
  }

  Future<String> get(key) async {
    SharedPreferences _prefs = await SharedPreferences.getInstance();
    String value = _prefs.getString(key) ?? null;
    return value;
  }

  Future<void> remove(key) async {
    SharedPreferences _prefs = await SharedPreferences.getInstance();
    _prefs.remove(key);
  }

  Future<bool> isKeyExist(key) async {
    SharedPreferences _prefs = await SharedPreferences.getInstance();
    bool isKeyExist = _prefs.containsKey(key);
    return isKeyExist;
  }

  Future<void> logout() async {
    SharedPreferences _prefs = await SharedPreferences.getInstance();
    _prefs.remove('access_token');
    _prefs.remove('full_number');
    _prefs.remove('user_id');
  }
}
