import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;

class HttpService {
  static final String api_url = "http://10.0.2.2:8086/api";
  // static final String api_url = "https://zyleapi.herokuapp.com";

  static Future<String> checkAPIConnection() async {
    try {
      var res = await http.get(api_url);
      if (res.statusCode == 200) {
        return res.body;
      } else {
        return "Failed to get response";
      }
    } catch (e) {
      return "Failed to connect server";
    }
  }

  static Future<bool> signupUser(user) async {
    var uri = api_url + '/user/signup';
    print("from https");
    String jsonUser = jsonEncode(user);

    try {
      var res = await http.post(uri,
          headers: {
            'Content-type': 'application/json',
            'Accept': 'application/json',
          },
          body: jsonEncode(
            <String, String>{'user_data': '$jsonUser'},
          ));
      if (res.statusCode == 200) {
        Map<String, dynamic> data =
            jsonDecode(res.body); // import 'dart:convert';

        return data['user_registered'];
      } else {
        return false;
      }
    } catch (e) {
      return false;
    }
  }

  static Future<bool> getOTP(number) async {
    final uri = api_url + '/user/getotp';
    // print('number');
    print(jsonEncode(number));
    String jsonNumber = jsonEncode(number);
    try {
      var res = await http.post(uri,
          headers: {
            'Content-type': 'application/json',
            'Accept': 'application/json',
          },
          body: jsonEncode(
            <String, String>{'number': '$jsonNumber'},
          ));
      if (res.statusCode == 200) {
        Map<String, dynamic> data =
            jsonDecode(res.body); // import 'dart:convert';

        return data['otp_sent'];
        // return true;
      } else {
        print(res.statusCode);
        return false;
        // return true;
      }
    } catch (e) {
      print(e);
      return false;
      // return true;
    }
  }

  static Future<bool> verifyOTP(number, pin) async {
    var uri = api_url + '/user/verifyotp';
    String jsonNumber = jsonEncode(number);

    try {
      var res = await http.post(uri,
          headers: {
            'Content-type': 'application/json',
            'Accept': 'application/json',
          },
          body: jsonEncode(
            <String, String>{'number': '$jsonNumber', 'pin': '$pin'},
          ));
      if (res.statusCode == 200) {
        Map<String, dynamic> data =
            jsonDecode(res.body); // import 'dart:convert';

        return data['otp_verified'];
        // return true;
      } else {
        return false;
        // return true;
      }
    } catch (e) {
      return false;
      // return true;
    }
  }

  static Future<bool> isAlreadyRegistered(number) async {
    var uri = api_url + '/user/already_registered';
    String jsonNumber = jsonEncode(number);
    print('registering check...');
    try {
      var res = await http.post(uri,
          headers: {
            'Content-type': 'application/json',
            'Accept': 'application/json',
          },
          body: jsonEncode(
            <String, String>{'number': '$jsonNumber'},
          ));
      if (res.statusCode == 200) {
        Map<String, dynamic> data =
            jsonDecode(res.body); // import 'dart:convert';

        return data['already_registered'];
        // return false;
      } else {
        return false;
        // return true;
      }
    } catch (e) {
      return false;
      // return true;
    }
  }

  static Future<Map<String, dynamic>> getUserAuthToken(full_number) async {
    var uri = api_url + '/user/get_auth_token';
    print("get user token");
    try {
      var res = await http.post(uri,
          headers: {
            'Content-type': 'application/json',
            'Accept': 'application/json',
          },
          body: jsonEncode(
            <String, String>{'full_number': '$full_number'},
          ));
      if (res.statusCode == 200) {
        Map<String, dynamic> data =
            jsonDecode(res.body); // import 'dart:convert';
        // print(data[])
        return {
          'access_token': data['access_token'],
          'user_id': data['user_id']
        };
        // return false;
      } else {
        return {'access_token': null, 'user_id': null};
        // return true;
      }
    } catch (e) {
      return {'access_token': null, 'user_id': null};
      // return true;
    }
  }

  static Future<bool> authUser(access_token) async {
    var uri = api_url + '/user/auth';
    print("auth uuser");
    print(access_token);
    try {
      var res = await http.get(
        uri,
        headers: {
          'Content-type': 'application/json',
          'Accept': 'application/json',
          'Authorization': access_token
        },
      );
      // print(res.statusCode);
      if (res.statusCode == 200) {
        Map<String, dynamic> data =
            jsonDecode(res.body); // import 'dart:convert';
        return data['auth_user'];
        // return true;
      } else {
        return false;
      }
    } catch (e) {
      return false;
    }
  }

  static Future<bool> logoutUser(access_token) async {
    var uri = api_url + '/user/remove_auth_token';
    print("remove auth user");
    print(access_token);
    try {
      var res = await http.get(
        uri,
        headers: {
          'Content-type': 'application/json',
          'Accept': 'application/json',
          'Authorization': access_token
        },
      );
      // print(res.statusCode);
      if (res.statusCode == 200) {
        Map<String, dynamic> data =
            jsonDecode(res.body); // import 'dart:convert';

        return data['token_removed'];
        // return true;
      } else {
        return false;
      }
    } catch (e) {
      return false;
    }
  }
}
