class User {
  Map<String, String> number;
  String name;
  String dob;
  String email;
  String gender;
  String bio;
  List<String> photos = new List<String>();

  User({this.number});

  void set addName(String name) {
    this.name = name;
  }

  void set addDOB(String dob) {
    this.dob = dob;
  }

  void set addEmail(String email) {
    this.email = email;
  }

  void set addGender(String gender) {
    this.gender = gender;
  }

  void set addBio(String bio) {
    this.bio = bio;
  }

  void addFirstPhoto(String photo) {
    this.photos.add(photo);
  }

  Map<String, dynamic> get userInfo {
    return {
      "number": number,
      "name": name,
      "dob": dob,
      "email": email,
      'gender': gender,
      "bio": bio,
      "photos": photos
    };
  }

  Map<String, dynamic> toJson() => {
        "number": number,
        "name": name,
        "dob": dob,
        "email": email,
        'gender': gender,
        "bio": bio,
        "photos": photos
      };
}