// import 'package:flutter/material.dart';
// import 'package:ZYLE/models/message_model.dart';
// import 'package:ZYLE/models/user_model1.dart';

// class ChatRoom extends StatefulWidget {
//   final User user;

//   ChatRoom({this.user});

//   @override
//   _ChatRoomState createState() => _ChatRoomState();
// }

// class _ChatRoomState extends State<ChatRoom> {
//   // final senderId = "7a31c0b1-bf34-4830-970a-9ac335ad22cc";
//   // final receiverId = "a18751fd-e33a-4899-9cf9-b7bad358b104";

//   // void sendMsg(senderId, receiverId, msg) async {
//   //   var isStored = await FirebaseService.storeMsg(senderId, receiverId, msg);
//   //   var isSent = await FirebaseService.sendMsg(senderId, receiverId, msg);
//   //   if (isStored && isSent) {
//   //     print("msg sent");
//   //   } else {
//   //     print("faiiled to send msg");
//   //   }
//   // }
//   _chatBubble(Message message, bool isMe, bool isSameUser) {
//     if (isMe) {
//       return Column(
//         children: <Widget>[
//           Container(
//             alignment: Alignment.topRight,
//             child: Container(
//               constraints: BoxConstraints(
//                 maxWidth: MediaQuery.of(context).size.width * 0.80,
//               ),
//               padding: EdgeInsets.all(10),
//               margin: EdgeInsets.symmetric(vertical: 10),
//               decoration: BoxDecoration(
//                 color: Color(0xffb29fff),
//                 borderRadius: BorderRadius.circular(15),
//                 boxShadow: [
//                   BoxShadow(
//                     color: Colors.grey.withOpacity(0.5),
//                     spreadRadius: 2,
//                     blurRadius: 5,
//                   ),
//                 ],
//               ),
//               child: Text(
//                 message.text,
//                 style: TextStyle(
//                   color: Colors.white,
//                 ),
//               ),
//             ),
//           ),
//           !isSameUser
//               ? Row(
//                   mainAxisAlignment: MainAxisAlignment.end,
//                   children: <Widget>[
//                     Text(
//                       message.time,
//                       style: TextStyle(
//                         fontSize: 12,
//                         color: Colors.black45,
//                       ),
//                     ),
//                     SizedBox(
//                       width: 10,
//                     ),
//                     Container(
//                       decoration: BoxDecoration(
//                         shape: BoxShape.circle,
//                         boxShadow: [
//                           BoxShadow(
//                             color: Colors.grey.withOpacity(0.5),
//                             spreadRadius: 2,
//                             blurRadius: 5,
//                           ),
//                         ],
//                       ),
//                       child: CircleAvatar(
//                         radius: 15,
//                         backgroundImage: AssetImage(message.sender.imageUrl),
//                       ),
//                     ),
//                   ],
//                 )
//               : Container(
//                   child: null,
//                 ),
//         ],
//       );
//     } else {
//       return Column(
//         children: <Widget>[
//           Container(
//             alignment: Alignment.topLeft,
//             child: Container(
//               constraints: BoxConstraints(
//                 maxWidth: MediaQuery.of(context).size.width * 0.80,
//               ),
//               padding: EdgeInsets.all(10),
//               margin: EdgeInsets.symmetric(vertical: 10),
//               decoration: BoxDecoration(
//                 color: Colors.white,
//                 borderRadius: BorderRadius.circular(15),
//                 boxShadow: [
//                   BoxShadow(
//                     color: Colors.grey.withOpacity(0.5),
//                     spreadRadius: 2,
//                     blurRadius: 5,
//                   ),
//                 ],
//               ),
//               child: Text(
//                 message.text,
//                 style: TextStyle(
//                   color: Colors.black54,
//                 ),
//               ),
//             ),
//           ),
//           !isSameUser
//               ? Row(
//                   children: <Widget>[
//                     Container(
//                       decoration: BoxDecoration(
//                         shape: BoxShape.circle,
//                         boxShadow: [
//                           BoxShadow(
//                             color: Colors.grey.withOpacity(0.5),
//                             spreadRadius: 2,
//                             blurRadius: 5,
//                           ),
//                         ],
//                       ),
//                       child: CircleAvatar(
//                         radius: 15,
//                         backgroundImage: AssetImage(message.sender.imageUrl),
//                       ),
//                     ),
//                     SizedBox(
//                       width: 10,
//                     ),
//                     Text(
//                       message.time,
//                       style: TextStyle(
//                         fontSize: 12,
//                         color: Colors.black45,
//                       ),
//                     ),
//                   ],
//                 )
//               : Container(
//                   child: null,
//                 ),
//         ],
//       );
//     }
//   }

//   _sendMessageArea() {
//     return Container(
//       padding: EdgeInsets.symmetric(horizontal: 8),
//       height: 70,
//       color: Colors.white,
//       child: Row(
//         children: <Widget>[
//           IconButton(
//             icon: Icon(Icons.photo),
//             iconSize: 25,
//             color: Theme.of(context).primaryColor,
//             onPressed: () {},
//           ),
//           Expanded(
//             child: TextField(
//               decoration: InputDecoration.collapsed(
//                 hintText: 'Send a message..',
//               ),
//               textCapitalization: TextCapitalization.sentences,
//             ),
//           ),
//           IconButton(
//             icon: Icon(Icons.send),
//             iconSize: 25,
//             color: Theme.of(context).primaryColor,
//             onPressed: () {},
//           ),
//         ],
//       ),
//     );
//   }

//   @override
//   Widget build(BuildContext context) {
//     int prevUserId;
//     return Scaffold(
//       backgroundColor: Color(0xFFF6F6F6),
//       appBar: AppBar(
//         brightness: Brightness.dark,
//         centerTitle: true,
//         title: RichText(
//           textAlign: TextAlign.center,
//           text: TextSpan(
//             children: [
//               TextSpan(
//                   text: widget.user.name,
//                   style: TextStyle(
//                     fontSize: 16,
//                     fontWeight: FontWeight.w400,
//                   )),
//               TextSpan(text: '\n'),
//               widget.user.isOnline
//                   ? TextSpan(
//                       text: 'Online',
//                       style: TextStyle(
//                         fontSize: 11,
//                         fontWeight: FontWeight.w400,
//                       ),
//                     )
//                   : TextSpan(
//                       text: 'Offline',
//                       style: TextStyle(
//                         fontSize: 11,
//                         fontWeight: FontWeight.w400,
//                       ),
//                     )
//             ],
//           ),
//         ),
//         leading: IconButton(
//             icon: Icon(Icons.arrow_back_ios),
//             color: Colors.white,
//             onPressed: () {
//               Navigator.pop(context);
//             }),
//       ),
//       body: StreamBuilder(
//           stream: null,
//           builder: (context, snapshot) {
//             //   if (snapshot.data == null) {
//             //   return Center(
//             //     child: RaisedButton(
//             //       onPressed: () {
//             //         // sendMsg(senderId, receiverId, "hello from sarvottam");
//             //         print("btn click");
//             //       },
//             //       child: Text("no data foudn"),
//             //     ),
//             //   );
//             // }
//             return Column(
//               children: <Widget>[
//                 Expanded(
//                   child: ListView.builder(
//                     reverse: true,
//                     padding: EdgeInsets.all(20),
//                     itemCount: messages.length,
//                     // itemCount: snapshot.data.documents.length,

//                     itemBuilder: (BuildContext context, int index) {
//                       final Message message = messages[index];
//                       final bool isMe = message.sender.id == currentUser.id;
//                       final bool isSameUser = prevUserId == message.sender.id;
//                       prevUserId = message.sender.id;
//                       return _chatBubble(message, isMe, isSameUser);
//                     },
//                   ),
//                 ),
//                 _sendMessageArea(),
//               ],
//             );
//           }),
//     );
//   }
// }
