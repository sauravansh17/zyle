import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:intl_phone_field/intl_phone_field.dart';
import 'package:ZYLE/utils/screen_size.dart';

class VerifyNumber extends StatelessWidget {
  final Function _getOTP;
  final verifyNumberController = TextEditingController();
  final verifyNumberKey = GlobalKey<ScaffoldState>();

  ScreenSize _ss;
  bool _isClicked;

  Map<String, String> number = {
    'number': '',
    'countryCode': '',
    'countryISOCode': ''
  };
  VerifyNumber(this._getOTP, this._isClicked);

  void _showSnackbar(String message) {
    verifyNumberKey.currentState.showSnackBar(new SnackBar(
      content: new Text(message),
    ));
  }

  @override
  Widget build(BuildContext context) {
    // print(type);
    _ss = ScreenSize(context);

    return MaterialApp(
      title: "Verify number",
      home: Scaffold(
        key: verifyNumberKey,
        backgroundColor: Color(0xffb29fff),
        body: SingleChildScrollView(
          child: Column(
            children: [
              Center(
                child: Container(
                  child: Image(
                    image: AssetImage('assets/images/cv.png'),
                    height: _ss.sH(30),
                    width: _ss.sW(50),
                  ),
                ),
              ),
              Container(
                  alignment: Alignment.centerLeft,
                  padding: EdgeInsets.only(left: _ss.sW(10)),
                  child: SizedBox(
                    width: _ss.sW(50),
                    child: Text(
                      "Verify Your Number",
                      style: TextStyle(
                          fontSize: _ss.sH(4),
                          color: Color(0xff555555),
                          fontWeight: FontWeight.w700),
                    ),
                  )),
              Container(
                alignment: Alignment.centerLeft,
                padding: EdgeInsets.only(left: _ss.sW(10)),
                child: SizedBox(
                  width: _ss.sW(90),
                  child: Text(
                    "We protect our community by making sure everyone on VC is real.",
                    style: TextStyle(
                      color: Color(0xff4e4e4e),
                      fontSize: _ss.sH(2.5),
                      fontFamily: "Montserrat",
                      fontWeight: FontWeight.w400,
                    ),
                  ),
                ),
              ),
              Container(
                alignment: Alignment.center,
                padding: EdgeInsets.only(
                    left: _ss.sW(10), top: _ss.sH(5), right: _ss.sW(5)),
                child: IntlPhoneField(
                  textAlign: TextAlign.center,
                  controller: verifyNumberController,
                  inputFormatters: [
                    LengthLimitingTextInputFormatter(10),
                    new FilteringTextInputFormatter.allow(RegExp("[0-9]"))
                  ],
                  style: TextStyle(
                    height: 1.4,
                    color: Colors.black,

                    // fontSize: 20,
                  ),
                  dropDownArrowColor: Color(0xffb4b4b4),
                  dropdownDecoration: BoxDecoration(
                      color: Color(0xfff3f3f3),
                      borderRadius: BorderRadius.circular(10)),
                  decoration: InputDecoration(
                    // labelText: 'Phone Number',
                    hintText: 'Phone Number',
                    filled: true,
                    fillColor: Color(0xfff3f3f3),
                    contentPadding:
                        EdgeInsets.symmetric(vertical: 10.0, horizontal: 20.0),
                    border: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(10),
                      borderSide: BorderSide(style: BorderStyle.none),
                    ),
                  ),
                  initialCountryCode: 'IN',
                  onChanged: (phone) {
                    number.update('number', (value) => value = phone.number);
                    number.update(
                        'countryCode', (value) => value = phone.countryCode);
                    number.update('countryISOCode',
                        (value) => value = phone.countryISOCode);
                  },
                ),
              ),
              Container(
                width: _ss.sW(50),
                height: _ss.sH(7),
                margin: EdgeInsets.only(top: _ss.sH(15)),
                child: RaisedButton(
                  onPressed: _isClicked
                      ? null
                      : () {
                          // print(verifyNumberController.text);
                          if (verifyNumberController.text == '' ||
                              verifyNumberController.text.length < 10) {
                            _showSnackbar("Enter 10-digit number!");
                          } else {
                            number.update('number',
                                (value) => value = verifyNumberController.text);
                            _getOTP(number);
                          }
                        },
                  textColor: Colors.white,
                  elevation: 5.0,
                  color: Color(0xff7e23b6),
                  padding: EdgeInsets.all(0.0),
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(20)),
                  child: Container(
                    // width: double.infinity,
                    padding: const EdgeInsets.all(10.0),
                    // alignment: Alignment.center,
                    child: Text("Get OTP"),
                  ),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
