import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import './login_screen.dart';
import 'package:ZYLE/services/app_storage/app_storage.dart';
import 'signup/signup_screen.dart';
import 'package:ZYLE/utils/screen_size.dart';

class LoginSignupScreen extends StatelessWidget {
  ScreenSize _ss;
  @override
  Widget build(BuildContext context) {
    _ss = ScreenSize(context);
    return MaterialApp(
        title: 'Login Signup Screen',
        home: Scaffold(
            body: Container(
          padding: EdgeInsets.only(left: _ss.sW(10), right: _ss.sW(10)),
          decoration: BoxDecoration(
            gradient: LinearGradient(
              begin: Alignment.topCenter,
              end: Alignment.bottomCenter,
              colors: [
                Color(0xff9824cf),
                Color(0xcd9f28b2),
                Color(0x9db128b4),
                Color(0xc4a849d5),
                Color(0xd8b365a2)
              ],
            ),
          ),
          child: Column(
            children: [
              Container(
                // color: Colors.amber,
                width: double.infinity,
                alignment: Alignment.topLeft,
                height: _ss.sH(25),
                child: Image(
                  image: AssetImage('assets/images/logo.png'),
                  height: _ss.sH(20),
                  width: _ss.sW(30),
                ),
              ),
              Container(
                  // color: Colors.red,
                  alignment: Alignment.topLeft,
                  height: _ss.sH(45),
                  width: double.infinity,
                  child: SizedBox(
                    width: _ss.sW(65),
                    child: Text(
                      'In this virtual world , you might find The ONE by coincidently meeting them here...',
                      textAlign: TextAlign.left,
                      style: TextStyle(
                        color: Colors.white,
                        fontSize: _ss.sH(4),
                        letterSpacing: 0,
                        fontWeight: FontWeight.bold,
                        // height: 1.3,
                      ),
                    ),
                  )),
              Container(
                height: _ss.sH(10),
                width: double.infinity,
                padding: EdgeInsets.all(10.0),
                child: RaisedButton(
                  onPressed: () {
                    Navigator.push(
                      context,
                      MaterialPageRoute(builder: (context) => LoginScreen()),
                    );
                  },
                  // padding: const EdgeInsets.all(0.0),
                  textColor: Colors.white,
                  elevation: 5.0,
                  color: Colors.black54,
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(20)),
                  child: Container(
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Icon(
                          Icons.dialpad_sharp,
                          color: Colors.white,
                          size: 15.0,
                        ),
                        Padding(
                          padding: EdgeInsets.symmetric(horizontal: 10.0),
                          child: Text('Log in using number',
                              textAlign: TextAlign.center,
                              style: TextStyle(
                                fontSize: _ss.sH(3),
                              )),
                        )
                      ],
                    ),
                  ),
                ),
              ),
              Container(
                width: double.infinity,
                height: _ss.sH(10),
                alignment: Alignment.center,
                child: Text(
                  'Terms & Condition',
                  // textAlign: TextAlign.left,
                  style: TextStyle(
                    color: Colors.white,
                    fontSize: 10,
                    letterSpacing: 0,
                    fontWeight: FontWeight.bold,
                    height: 1.3,
                  ),
                ),
              ),
              Container(
                  width: double.infinity,
                  height: _ss.sH(5),
                  alignment: Alignment.center,
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Text("Not registered? Here you can "),
                      RichText(
                          text: TextSpan(
                              text: "Sign up",
                              recognizer: TapGestureRecognizer()
                                ..onTap = () => {
                                      Navigator.push(
                                        context,
                                        MaterialPageRoute(
                                            builder: (context) =>
                                                SignupScreen()),
                                      )
                                    }))
                    ],
                  ))
            ],
          ),
        )));
  }
}
