import 'dart:convert';

import 'package:ZYLE/screens/signup/user_info_s2.dart';
import 'package:flutter/material.dart';
import 'package:ZYLE/utils/screen_size.dart';
import 'package:ZYLE/models/user_model.dart';
import 'package:flutter/services.dart';

class UserInfoS1 extends StatefulWidget {
  // final Function _getOTP;
  // final verifyNumberController = TextEditingController();
  Map<String, String> number = {
    'number': '',
    'countryCode': '',
    'countryISOCode': ''
  };
  UserInfoS1(
    this.number,
  );

  @override
  _UserInfoS1State createState() => _UserInfoS1State();
}

class _UserInfoS1State extends State<UserInfoS1> {
  ScreenSize _ss;
  User user;
  final userInfo1Key = GlobalKey<ScaffoldState>();

  final nameController = TextEditingController();
  final ddController = TextEditingController();
  final mmController = TextEditingController();
  final yyController = TextEditingController();
  final emailController = TextEditingController();

  @override
  void initState() {
    user = new User(number: widget.number);
    super.initState();
  }

  void _showSnackbar(String message) {
    userInfo1Key.currentState.showSnackBar(new SnackBar(
      content: new Text(message),
    ));
  }

  @override
  Widget build(BuildContext context) {
    _ss = ScreenSize(context);
    print(user.number);
    return MaterialApp(
      title: "User info page 1",
      home: Scaffold(
        key: userInfo1Key,
        backgroundColor: Color(0xfff4b4ff),
        body: SingleChildScrollView(
          child: Column(
            children: [
              Container(
                margin: EdgeInsets.only(top: _ss.sH(4)),
                alignment: Alignment.topLeft,
                child: Image(
                    image: AssetImage('assets/images/cv.png'),
                    height: _ss.sH(10),
                    width: _ss.sW(30)),
              ),
              Container(
                  alignment: Alignment.centerLeft,
                  margin: EdgeInsets.only(top: _ss.sH(5), left: _ss.sW(10)),
                  child: Text(
                    "What's your name?",
                    style: TextStyle(
                        fontSize: _ss.sH(3.5),
                        color: Color(0xff555555),
                        fontWeight: FontWeight.w700),
                  )),
              Container(
                alignment: Alignment.centerLeft,
                // margin: EdgeInsets.only(top: _ss.sH(5)),
                padding: EdgeInsets.only(left: _ss.sW(10)),
                child: SizedBox(
                  width: _ss.sW(90),
                  child: Text(
                    "You won't be able to change this later.",
                    style: TextStyle(
                      color: Color(0xff4e4e4e),
                      fontSize: _ss.sH(2.5),
                      fontWeight: FontWeight.w400,
                    ),
                  ),
                ),
              ),
              Container(
                alignment: Alignment.center,
                margin: EdgeInsets.only(
                    top: _ss.sH(1), right: _ss.sW(10), left: _ss.sW(10)),
                height: _ss.sH(7),
                child: TextFormField(
                  controller: nameController,
                  // inputFormatters: [LengthLimitingTextInputFormatter(25)],
                  validator: (value) {
                    if (value.isEmpty) {
                      return 'Please enter name';
                    }
                    return null;
                  },
                  decoration: InputDecoration(
                    hintText: "Enter name",
                    filled: true,
                    fillColor: Color(0xfff3f3f3),
                    border: new OutlineInputBorder(
                      borderRadius: new BorderRadius.circular(10.0),
                      borderSide: new BorderSide(),
                    ),
                  ),
                ),
              ),
              Container(
                  alignment: Alignment.centerLeft,
                  margin: EdgeInsets.only(
                      top: _ss.sH(4), right: _ss.sW(10), left: _ss.sW(10)),
                  child: Text(
                    "When's your birthday?",
                    style: TextStyle(
                        fontSize: _ss.sH(3.5),
                        color: Color(0xff555555),
                        fontWeight: FontWeight.w700),
                  )),
              Container(
                alignment: Alignment.center,
                margin: EdgeInsets.only(
                    top: _ss.sH(1), right: _ss.sW(10), left: _ss.sW(10)),
                height: _ss.sH(7),
                width: _ss.sW(80),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    SizedBox(
                      width: _ss.sW(15),
                      child: TextFormField(
                        controller: ddController,
                        keyboardType: TextInputType.number,
                        inputFormatters: [
                          LengthLimitingTextInputFormatter(2),
                          new FilteringTextInputFormatter.allow(RegExp("[0-9]"))
                        ],
                        validator: (value) {
                          if (value.isEmpty) {
                            return 'Please enter date';
                          }
                          return null;
                        },
                        decoration: InputDecoration(
                          hintText: "DD",
                          filled: true,
                          fillColor: Color(0xfff3f3f3),
                          border: new OutlineInputBorder(
                            borderRadius: new BorderRadius.circular(10.0),
                            borderSide: new BorderSide(),
                          ),
                        ),
                      ),
                    ),
                    SizedBox(
                      width: _ss.sW(15),
                      child: TextFormField(
                        controller: mmController,
                        keyboardType: TextInputType.number,
                        validator: (value) {
                          if (value.isEmpty) {
                            return 'Please enter some month';
                          }
                          return null;
                        },
                        inputFormatters: [
                          LengthLimitingTextInputFormatter(2),
                          new FilteringTextInputFormatter.allow(RegExp("[0-9]"))
                        ],
                        decoration: InputDecoration(
                          hintText: "MM",
                          filled: true,
                          fillColor: Color(0xfff3f3f3),
                          border: new OutlineInputBorder(
                            borderRadius: new BorderRadius.circular(10.0),
                            borderSide: new BorderSide(),
                          ),
                        ),
                      ),
                    ),
                    SizedBox(
                      width: _ss.sW(30),
                      child: TextFormField(
                        controller: yyController,
                        keyboardType: TextInputType.number,
                        validator: (value) {
                          if (value.isEmpty) {
                            return 'Please enter year';
                          }
                          return null;
                        },
                        inputFormatters: [
                          LengthLimitingTextInputFormatter(4),
                          new FilteringTextInputFormatter.allow(RegExp("[0-9]"))
                        ],
                        decoration: InputDecoration(
                          hintText: "YYYY",
                          filled: true,
                          fillColor: Color(0xfff3f3f3),
                          border: new OutlineInputBorder(
                            borderRadius: new BorderRadius.circular(10.0),
                            borderSide: new BorderSide(),
                          ),
                        ),
                      ),
                    )
                  ],
                ),
              ),
              Container(
                  alignment: Alignment.centerLeft,
                  margin: EdgeInsets.only(
                      top: _ss.sH(4), right: _ss.sW(10), left: _ss.sW(10)),
                  child: Text(
                    "What's your email address?",
                    style: TextStyle(
                        fontSize: _ss.sH(3.5),
                        color: Color(0xff555555),
                        fontWeight: FontWeight.w700),
                  )),
              Container(
                alignment: Alignment.center,
                margin: EdgeInsets.only(
                    top: _ss.sH(1), right: _ss.sW(10), left: _ss.sW(10)),
                height: _ss.sH(7),
                width: _ss.sW(80),
                child: TextFormField(
                  controller: emailController,
                  keyboardType: TextInputType.emailAddress,
                  inputFormatters: [],
                  validator: (value) {
                    if (value.isEmpty) {
                      return 'Please enter email';
                    }
                    return null;
                  },
                  decoration: InputDecoration(
                    hintText: "Enter email",
                    filled: true,
                    fillColor: Color(0xfff3f3f3),
                    border: new OutlineInputBorder(
                      borderRadius: new BorderRadius.circular(10.0),
                      borderSide: new BorderSide(),
                    ),
                  ),
                ),
              ),
              Container(
                width: _ss.sW(50),
                height: _ss.sH(7),
                margin: EdgeInsets.only(top: _ss.sH(5)),
                child: RaisedButton(
                  onPressed: () {
                    if (nameController.text == '' ||
                        mmController.text == '' ||
                        ddController.text == '' ||
                        emailController.text == '') {
                      _showSnackbar("Field is empty");
                    } else {
                      print(emailController);
                      user.addName = nameController.text;
                      user.addEmail = emailController.text;
                      user.addDOB = ddController.text +
                          '-' +
                          mmController.text +
                          '-' +
                          yyController.text;
                      print(user.userInfo);

                      Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) => UserInfoS2(user: user)),
                      );
                    }
                  },
                  // padding: const EdgeInsets.all(0.0),
                  textColor: Colors.white,
                  elevation: 5.0,
                  color: Color(0xff7e23b6),
                  padding: EdgeInsets.all(0.0),

                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(20)),
                  child: Container(
                    // width: double.infinity,
                    padding: const EdgeInsets.all(10.0),
                    // alignment: Alignment.center,
                    child: Text("Next"),
                  ),
                ),
              ),
              Container(
                alignment: Alignment.center,
                width: _ss.sW(80),
                height: _ss.sH(10),
                margin: EdgeInsets.only(top: _ss.sH(2)),
                child: RichText(
                  textAlign: TextAlign.left,
                  text: TextSpan(
                    children: [
                      WidgetSpan(
                        child: Icon(Icons.lock, size: 17),
                      ),
                      TextSpan(
                          text:
                              "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.",
                          style: TextStyle(
                              color: Colors.black, fontSize: _ss.sH(2))),
                    ],
                  ),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
