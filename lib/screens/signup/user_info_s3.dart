import 'package:ZYLE/screens/signup/user_info_s4.dart';
import 'package:flutter/material.dart';
import 'package:ZYLE/utils/screen_size.dart';
import 'package:ZYLE/models/user_model.dart';

class UserInfoS3 extends StatelessWidget {
  ScreenSize _ss;
  User user;
  final userInfo3Key = GlobalKey<ScaffoldState>();

  UserInfoS3({this.user});
  final bioController = TextEditingController();

  void _showSnackbar(String message) {
    userInfo3Key.currentState.showSnackBar(new SnackBar(
      content: new Text(message),
    ));
  }

  @override
  Widget build(BuildContext context) {
    _ss = ScreenSize(context);
    print(user);
    return MaterialApp(
      title: "Write bio",
      home: Scaffold(
        key: userInfo3Key,
        backgroundColor: Color(0xfff4b4ff),
        body: SingleChildScrollView(
          child: Column(
            children: [
              Container(
                margin: EdgeInsets.only(top: _ss.sH(4)),
                alignment: Alignment.topLeft,
                child: Image(
                  image: AssetImage('assets/images/cv.png'),
                  height: _ss.sH(10),
                  width: _ss.sW(30),
                ),
              ),
              Align(
                alignment: Alignment.centerLeft,
                child: Container(
                    margin: EdgeInsets.only(top: _ss.sH(5), left: _ss.sW(10)),
                    width: _ss.sW(50),
                    child: Text(
                      "Write about yourself",
                      style: TextStyle(
                          fontSize: _ss.sH(4),
                          color: Color(0xff555555),
                          fontWeight: FontWeight.w700),
                    )),
              ),
              Container(
                alignment: Alignment.center,
                margin: EdgeInsets.only(
                    top: _ss.sH(5), left: _ss.sW(10), right: _ss.sW(10)),
                // height: 50.0,
                child: TextFormField(
                  controller: bioController,
                  maxLines: 10,
                  validator: (value) {
                    if (value.isEmpty) {
                      return "Bio field is empty.";
                    }
                    return null;
                  },
                  decoration: InputDecoration(
                    hintText: "Write here...",
                    filled: true,
                    fillColor: Color(0xfff3f3f3),
                    border: new OutlineInputBorder(
                      borderRadius: new BorderRadius.circular(10.0),
                      borderSide: new BorderSide(),
                    ),
                  ),
                ),
              ),
              Container(
                width: _ss.sW(50),
                height: _ss.sH(7),
                margin: EdgeInsets.only(top: _ss.sH(5)),
                child: RaisedButton(
                  onPressed: () {
                    if (bioController.text == '') {
                      _showSnackbar("Enter bio.");
                    } else {
                      print(bioController.text);
                      user.addBio = bioController.text;
                      Navigator.push(
                        context,
                        MaterialPageRoute(builder: (context) => UserInfoS4(user:user)),
                      );
                    }
                  },
                  // padding: const EdgeInsets.all(0.0),
                  textColor: Colors.white,
                  elevation: 5.0,
                  color: Color(0xff7e23b6),
                  padding: EdgeInsets.all(0.0),

                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(20)),
                  child: Container(
                    // width: double.infinity,
                    padding: const EdgeInsets.all(10.0),
                    // alignment: Alignment.center,
                    child: Text("Next"),
                  ),
                ),
              ),
              Container(
                alignment: Alignment.center,
                width: _ss.sW(80),
                height: _ss.sH(10),
                margin: EdgeInsets.only(top: _ss.sH(2)),
                child: RichText(
                  textAlign: TextAlign.left,
                  text: TextSpan(
                    children: [
                      WidgetSpan(
                        child: Icon(Icons.lock, size: 17),
                      ),
                      TextSpan(
                          text:
                              "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.",
                          style: TextStyle(
                              color: Colors.black, fontSize: _ss.sH(2))),
                    ],
                  ),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
