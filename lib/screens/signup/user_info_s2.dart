import 'package:ZYLE/screens/signup/user_info_s3.dart';
import 'package:flutter/material.dart';
import 'package:ZYLE/utils/screen_size.dart';
import 'package:ZYLE/models/user_model.dart';

class UserInfoS2 extends StatefulWidget {
  User user;
  UserInfoS2({this.user});

  @override
  _UserInfoS2State createState() => _UserInfoS2State();
}

class _UserInfoS2State extends State<UserInfoS2> {
  int _radioValue = 0;

  String gender = 'male';

  ScreenSize _ss;

  void _handleRadioValueChange(int value) {
    setState(() {
      _radioValue = value;

      switch (_radioValue) {
        case 0:
          gender = 'male';
          break;
        case 1:
          gender = 'female';
          break;
        case 2:
          gender = 'non-binary';
          break;
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    _ss = ScreenSize(context);
    print(widget.user.userInfo);
    return MaterialApp(
      title: "Gender",
      home: Scaffold(
        backgroundColor: Color(0xfff4b4ff),
        body: SingleChildScrollView(
          child: Column(
            children: [
              Container(
                margin: EdgeInsets.only(top: _ss.sH(4)),
                alignment: Alignment.topLeft,
                child: Image(
                  image: AssetImage('assets/images/cv.png'),
                  height: _ss.sH(10),
                  width: _ss.sW(30),
                ),
              ),
              Container(
                  alignment: Alignment.centerLeft,
                  margin: EdgeInsets.only(top: _ss.sH(5), left: _ss.sW(10)),
                  child: Text(
                    "How do you identify yourself?",
                    style: TextStyle(
                        fontSize: _ss.sH(3.5),
                        color: Color(0xff555555),
                        fontWeight: FontWeight.w700),
                  )),
              Container(
                alignment: Alignment.centerLeft,
                margin: EdgeInsets.only(top: _ss.sH(2), left: _ss.sW(10)),
                child: SizedBox(
                  width: _ss.sW(80),
                  child: Text(
                    "Zyle welcome everyone.",
                    style: TextStyle(
                      color: Color(0xff4e4e4e),
                      fontSize: _ss.sH(2.5),
                      fontWeight: FontWeight.w400,
                    ),
                  ),
                ),
              ),
              Container(
                margin: EdgeInsets.only(top: _ss.sH(5), left: _ss.sW(10)),
                // height: 50.0,

                child: Column(
                  children: <Widget>[
                    Row(
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: [
                        SizedBox(
                          width: _ss.sW(70),
                          child: Text(
                            "Male",
                            style: TextStyle(
                              fontSize: _ss.sH(3),
                              color: Color(0xff555555),
                            ),
                          ),
                        ),
                        Radio(
                            value: 0,
                            groupValue: _radioValue,
                            onChanged: _handleRadioValueChange),
                      ],
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: [
                        SizedBox(
                          width: _ss.sW(70),
                          child: Text(
                            "Female",
                            style: TextStyle(
                              fontSize: _ss.sH(3),
                              color: Color(0xff555555),
                            ),
                          ),
                        ),
                        Radio(
                            value: 1,
                            groupValue: _radioValue,
                            onChanged: _handleRadioValueChange),
                      ],
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: [
                        SizedBox(
                          width: _ss.sW(70),
                          child: Text(
                            "Non-binary",
                            style: TextStyle(
                              fontSize: _ss.sH(3),
                              color: Color(0xff555555),
                            ),
                          ),
                        ),
                        Radio(
                            value: 2,
                            groupValue: _radioValue,
                            onChanged: _handleRadioValueChange),
                      ],
                    )
                  ],
                ),
              ),
              Container(
                width: _ss.sW(50),
                height: _ss.sH(7),
                margin: EdgeInsets.only(top: _ss.sH(5)),
                child: RaisedButton(
                  onPressed: () {
                    print(gender);
                    widget.user.addGender = gender;
                    print(widget.user.userInfo);
                    Navigator.push(
                      context,
                      MaterialPageRoute(
                          builder: (context) => UserInfoS3(user: widget.user)),
                    );
                  },
                  // padding: const EdgeInsets.all(0.0),
                  textColor: Colors.white,
                  elevation: 5.0,
                  color: Color(0xff7e23b6),
                  padding: EdgeInsets.all(0.0),

                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(20)),
                  child: Container(
                    // width: double.infinity,
                    padding: const EdgeInsets.all(10.0),
                    // alignment: Alignment.center,
                    child: Text("Next"),
                  ),
                ),
              ),
              Container(
                alignment: Alignment.center,
                width: _ss.sW(80),
                height: _ss.sH(10),
                margin: EdgeInsets.only(top: _ss.sH(2)),
                child: RichText(
                  textAlign: TextAlign.left,
                  text: TextSpan(
                    children: [
                      WidgetSpan(
                        child: Icon(Icons.lock, size: 17),
                      ),
                      TextSpan(
                          text:
                              "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.",
                          style: TextStyle(
                              color: Colors.black, fontSize: _ss.sH(2))),
                    ],
                  ),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
