import 'dart:convert';

import 'package:ZYLE/screens/login_signup_screen.dart';
import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';
import 'dart:io';
import 'package:ZYLE/utils/screen_size.dart';
import 'package:ZYLE/models/user_model.dart';
import 'package:ZYLE/services/http_service/http_service.dart';

class UserInfoS4 extends StatefulWidget {
  User user;
  UserInfoS4({this.user});
  @override
  _UserInfoS4State createState() => _UserInfoS4State();
}

class _UserInfoS4State extends State<UserInfoS4> {
  File _image;
  String _image_path;
  bool submitted = false;
  final picker = ImagePicker();
  ScreenSize _ss;
  final userInfo4Key = GlobalKey<ScaffoldState>();
  bool image_selected = false;

  void _showSnackbar(String message) {
    userInfo4Key.currentState.showSnackBar(new SnackBar(
      content: new Text(message),
    ));
  }

  Future getImage(String source) async {
    var pickedFile;
    try {
      if (source == 'gallery') {
        pickedFile = await picker.getImage(source: ImageSource.gallery);
      } else {
        pickedFile = await picker.getImage(source: ImageSource.camera);
      }
      print('file retrieved.');
    } catch (e) {
      print("Exception error");
    }

    setState(() {
      if (pickedFile != null) {
        print('picked file');
        print(pickedFile);
        _image = File(pickedFile.path);
        _image_path = pickedFile.path;
        image_selected = true;
        print(_image);
      } else {
        _showSnackbar("No image selected.");
        print('No image selected.');
      }
    });
  }

  Future<void> _showSignupSuccessDialog() async {
    return showDialog<void>(
      context: context,
      barrierDismissible: false, // user must tap button!
      builder: (BuildContext context) {
        return AlertDialog(
          title: Text('You Signed up successfully!! '),
          content: SingleChildScrollView(
            child: ListBody(
              children: <Widget>[
                Text('You can log in now.'),
              ],
            ),
          ),
          actions: <Widget>[
            TextButton(
                child: Text('OK'),
                onPressed: () {
                  Navigator.pushAndRemoveUntil(
                      context,
                      MaterialPageRoute(
                          builder: (context) => LoginSignupScreen()),
                      (Route<dynamic> route) => false);
                  
                }),
          ],
        );
      },
    );
  }

  Future<void> _showSignupFailDialog() async {
    return showDialog<void>(
      context: context,
      barrierDismissible: false, // user must tap button!
      builder: (BuildContext context) {
        return AlertDialog(
          title: Text('Failed to sign up'),
          // content: SingleChildScrollView(
          //   child: ListBody(
          //     children: <Widget>[
          //       Text('Fail to login.'),
          //     ],
          //   ),
          // ),
          actions: <Widget>[
            TextButton(
              child: Text('OK'),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
          ],
        );
      },
    );
  }

  void _createAccount() async {
    String base64Image = base64Encode(_image.readAsBytesSync());
    print(base64Image);
    if (widget.user.photos.isEmpty) {
      // widget.user.addFirstPhoto(_image_path);
      widget.user.addFirstPhoto(base64Image);
    } else {
      widget.user.photos.clear();
      // widget.user.addFirstPhoto(_image_path);
      widget.user.addFirstPhoto(base64Image);

    }
    setState(() {
      submitted = true;
    });
    print(widget.user.userInfo);

    // _showSnackbar("Creating account...");
    try {
      print("create account");
      bool user_registered = await HttpService.signupUser(widget.user);
      print(user_registered);
      if (user_registered) {
        _showSignupSuccessDialog();
      } else {
        setState(() {
          submitted = false;
        });
        _showSignupFailDialog();
      }
    } catch (e) {
      setState(() {
        submitted = false;
      });
      _showSignupFailDialog();
    }
  }

  @override
  Widget build(BuildContext context) {
    _ss = ScreenSize(context);
    print(widget.user.userInfo);
    return MaterialApp(
      title: "Add photo",
      home: Scaffold(
        key: userInfo4Key,
        backgroundColor: Color(0xfff4b4ff),
        body: SingleChildScrollView(
          child: Column(
            children: [
              Container(
                margin: EdgeInsets.only(top: _ss.sH(4)),
                alignment: Alignment.topLeft,
                child: Image(
                  image: AssetImage('assets/images/cv.png'),
                  height: _ss.sH(10),
                  width: _ss.sW(30),
                ),
              ),
              Align(
                alignment: Alignment.centerLeft,
                child: Container(
                    alignment: Alignment.centerLeft,
                    margin: EdgeInsets.only(top: _ss.sH(5), left: _ss.sW(10)),
                    child: Text(
                      "Add your first photo",
                      style: TextStyle(
                          fontSize: _ss.sH(4),
                          color: Color(0xff555555),
                          fontWeight: FontWeight.w700),
                    )),
              ),
              Container(
                  alignment: Alignment.centerLeft,
                  margin: EdgeInsets.only(
                      top: _ss.sH(1), left: _ss.sW(10), right: _ss.sW(1.5)),
                  child: Text(
                      "Choose a photo of just your where you can clearly see your face. You can change this later.",
                      style: TextStyle(
                        fontSize: _ss.sH(2.5),
                        color: Color(0xff555555),
                      ))),
              Container(
                width: _ss.sW(80),
                height: _ss.sH(7),
                margin: EdgeInsets.only(top: _ss.sH(4)),
                child: RaisedButton(
                  onPressed: submitted
                      ? null
                      : () {
                          showModalBottomSheet<void>(
                            context: context,
                            builder: (BuildContext context) {
                              return SafeArea(
                                child: Container(
                                  child: new Wrap(
                                    children: <Widget>[
                                      new ListTile(
                                          leading: new Icon(Icons.photo_camera),
                                          title: new Text('Camera'),
                                          onTap: () {
                                            getImage('Take a photo');
                                            Navigator.of(context).pop();
                                          }),
                                      new ListTile(
                                        leading: new Icon(Icons.photo_library),
                                        title: new Text('Phone Gallery'),
                                        onTap: () {
                                          getImage('gallery');
                                          Navigator.of(context).pop();
                                        },
                                      ),
                                    ],
                                  ),
                                ),
                              );
                            },
                          );
                        },
                  // padding: const EdgeInsets.all(0.0),
                  textColor: Colors.white,
                  elevation: 5.0,
                  color: Color(0xff7e23b6),
                  padding: EdgeInsets.all(0.0),
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(20)),
                  child: Container(
                    // width: double.infinity,
                    padding: const EdgeInsets.all(10.0),
                    // alignment: Alignment.center,
                    child: Text(
                      "Choose a photo",
                      style: TextStyle(fontSize: 15.0),
                    ),
                  ),
                ),
              ),
              Container(
                margin: EdgeInsets.only(top: _ss.sH(3)),
                width: _ss.sW(80),
                height: _ss.sH(25),
                child: Center(
                    child: _image == null
                        ? Text('No image selected.')
                        : Image.file(_image)),
              ),
              Container(
                width: _ss.sW(50),
                height: _ss.sH(7),
                margin: EdgeInsets.only(top: _ss.sH(5)),
                child: RaisedButton(
                  onPressed: submitted
                      ? null
                      : () {
                          print(widget.user.userInfo);
                          if (image_selected) {
                            _createAccount();
                          } else {
                            _showSnackbar("No image selected.");
                          }
                        },
                  // padding: const EdgeInsets.all(0.0),
                  textColor: Colors.white,
                  elevation: 5.0,
                  color: Color(0xff7e23b6),
                  padding: EdgeInsets.all(0.0),

                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(20)),
                  child: Container(
                    // width: double.infinity,
                    padding: const EdgeInsets.all(10.0),
                    // alignment: Alignment.center,
                    child: Text("Submit"),
                  ),
                ),
              ),
              Container(
                alignment: Alignment.center,
                width: _ss.sW(80),
                height: _ss.sH(10),
                margin: EdgeInsets.only(top: _ss.sH(2)),
                child: RichText(
                  textAlign: TextAlign.left,
                  text: TextSpan(
                    children: [
                      WidgetSpan(
                        child: Icon(Icons.lock, size: 17),
                      ),
                      TextSpan(
                          text:
                              "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.",
                          style: TextStyle(
                              color: Colors.black, fontSize: _ss.sH(2))),
                    ],
                  ),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
