import 'package:ZYLE/screens/login_signup_screen.dart';
import 'package:ZYLE/screens/verify_number.dart';
import 'package:ZYLE/screens/verify_otp.dart';
import 'package:flutter/material.dart';
import 'package:ZYLE/services/http_service/http_service.dart';
import 'package:ZYLE/models/user_model.dart';

class SignupScreen extends StatefulWidget {
  @override
  _SignupScreenState createState() => _SignupScreenState();
}

class _SignupScreenState extends State<SignupScreen> {
  final GlobalKey<ScaffoldState> _snackbarKey = new GlobalKey<ScaffoldState>();
  bool _isClicked = false;
  @override
  void initState() {
    // initializeFlutterFire();
    super.initState();
  }

  @override
  void dispose() {
    // signupFormController.dispose();
    super.dispose();
  }

  void _showSnackbar(String message) {
    _snackbarKey.currentState.showSnackBar(SnackBar(
      content: Text(message),
    ));
  }

  Future<void> _showSignupSuccessDialog() async {
    return showDialog<void>(
      context: context,
      barrierDismissible: false, // user must tap button!
      builder: (BuildContext context) {
        return AlertDialog(
          title: Text('Sign up successfull!! '),
          content: SingleChildScrollView(
            child: ListBody(
              children: <Widget>[
                Text('You can log in now.'),
              ],
            ),
          ),
          actions: <Widget>[
            TextButton(
                child: Text('OK'),
                onPressed: () {
                  Navigator.pushAndRemoveUntil(
                      context,
                      MaterialPageRoute(
                          builder: (context) => LoginSignupScreen()),
                      (Route<dynamic> route) => false);
                  //   Navigator.pushNamedAndRemoveUntil(
                  //       context, '/home', (Route<dynamic> route) => true);
                  // },
                }),
          ],
        );
      },
    );
  }

  Future<void> _showSignupFailDialog() async {
    return showDialog<void>(
      context: context,
      barrierDismissible: false, // user must tap button!
      builder: (BuildContext context) {
        return AlertDialog(
          title: Text('Fail to sign up'),
          // content: SingleChildScrollView(
          //   child: ListBody(
          //     children: <Widget>[
          //       Text('Fail to login.'),
          //     ],
          //   ),
          // ),
          actions: <Widget>[
            TextButton(
              child: Text('OK'),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
          ],
        );
      },
    );
  }

  void submitSignupForm(number) async {
    print(number);
    // var response = await HttpService.checkAPIConnection();
    var response = await HttpService.signupUser(number);
    print('output: ${response}');
    if (response) {
      _showSignupSuccessDialog();
    } else {
      _showSignupFailDialog();
    }
  }

/**
 * Map<String,String> number={'number','countrycode','countryISOCode'}
 * 
 */

  Future<bool> _isAlreadyRegistered(number) async {
    try {
      bool isAlreadyRegistered = await HttpService.isAlreadyRegistered(number);
      // print(isAlreadyRegistered);
      return isAlreadyRegistered;
    } catch (e) {
      return false;
    }
  }

  void _getOTP(number) async {
    print('get otp from signup screen');

//  Navigator.pushReplacement(
//                 context,
//                 MaterialPageRoute(
//                     builder: (BuildContext context) => VerifyOTP(
//                           number: number,
//                           type: 'signup',
//                         )));

    setState(() {
      _isClicked = true;
    });
    try {
      bool registered = await _isAlreadyRegistered(number);
      if (registered) {
        _showSnackbar("You are already registered. You can log in.");
        setState(() {
          _isClicked = false;
        });
      } else {
        try {
          bool otp_sent = await HttpService.getOTP(number);
          if (otp_sent) {
            print("OTP sent");
            Navigator.pushReplacement(
                context,
                MaterialPageRoute(
                    builder: (BuildContext context) => VerifyOTP(
                          number: number,
                          type: 'signup',
                        )));
          } else {
            print("Failed to send OTP.");
            _showSnackbar("Failed to send OTP.");
            setState(() {
              _isClicked = false;
            });
          }
        } catch (e) {
          print("Exception Error");
          _showSnackbar("Failed to send OTP.");
          setState(() {
            _isClicked = false;
          });
        }
      }
    } catch (e) {
      print("Exception Error");
      _showSnackbar("Failed to send OTP.");
      setState(() {
        _isClicked = false;
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        // appBar: AppBar(title: Text("Sign Up")),
        key: _snackbarKey,
        resizeToAvoidBottomInset: false,
        body: VerifyNumber(_getOTP, _isClicked));
  }
}
