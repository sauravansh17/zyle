import 'package:ZYLE/screens/chat_room.dart';
import 'package:ZYLE/screens/landing_page.dart';
import 'package:ZYLE/screens/login_signup_screen.dart';
import 'package:ZYLE/screens/test_chat_room.dart';
import 'package:ZYLE/screens/signup/user_info_s1.dart';
import 'package:ZYLE/screens/signup/user_info_s2.dart';
import 'package:ZYLE/screens/signup/user_info_s3.dart';
import 'package:ZYLE/screens/signup/user_info_s4.dart';
import 'package:ZYLE/screens/splash_screen.dart';
import 'package:ZYLE/screens/user/home_screen.dart';
import 'package:ZYLE/screens/user/profile_screen.dart';
import 'package:ZYLE/screens/verify_otp.dart';
import 'package:flutter/material.dart';
import 'package:ZYLE/screens/verify_number.dart';
import 'package:firebase_core/firebase_core.dart';

class TestScreen extends StatefulWidget {
  @override
  _TestScreenState createState() => _TestScreenState();
}

class _TestScreenState extends State<TestScreen> {
  // bool _firebase_initialized = false;
  // bool _firebase_error = false;

  // void initializeFlutterFire() async {
  //   try {
  //     await Firebase.initializeApp();
  //     setState(() {
  //       _firebase_initialized = true;
  //     });
  //   } catch (e) {
  //     setState(() {
  //       _firebase_error = true;
  //     });
  //   }
  // }

  // @override
  // void initState() {
  //   initializeFlutterFire();
  //   super.initState();
  // }

  @override
  Widget build(BuildContext context) {
    // return Center(
    //   child: Text("Test Screen"),
    // );
    // return UserInfoS1(
    //     {'number': '9958938566', 'countryCode': '+91', 'countryISOCode': 'IN'});
    // return UserInfoS4(user: user);

    // return Image.network(
    //   'https://zyle.s3.amazonaws.com/profile2.jpg',
    //   height: 100,
    //   width: 100,
    // );

    // if (_firebase_error) {
    //   return Center(
    //     child: Text("error"),
    //   );
    // }

    // if (!_firebase_initialized) {
    //   return Center(
    //     child: Text("loading..."),
    //   );
    // }
    return LandingPage('+919958938566');
    // return VerifyOTP(number: {
    //   'number': '9958938566',
    //   'countryCode': '+91',
    //   'countryISOCode': 'IN'
    // }, type: 'login');
  }
}
