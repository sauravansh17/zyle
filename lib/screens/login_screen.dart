import 'package:ZYLE/screens/landing_page.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import '../services/http_service/http_service.dart';
import 'package:ZYLE/screens/user/home_screen.dart';
import 'package:ZYLE/screens/verify_number.dart';
import 'package:ZYLE/services/http_service/http_service.dart';
import 'package:ZYLE/screens/verify_otp.dart';

class LoginScreen extends StatefulWidget {
  @override
  _LoginScreenState createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {
  final GlobalKey<ScaffoldState> _snackbarKey = new GlobalKey<ScaffoldState>();
  bool _isClicked = false;
  @override
  void initState() {
    super.initState();
  }

  @override
  void dispose() {
    // signupFormController.dispose();
    super.dispose();
  }

  void _showSnackbar(String message) {
    _snackbarKey.currentState.showSnackBar(SnackBar(
      content: Text(message),
    ));
  }

  Future<bool> _isAlreadyRegistered(number) async {
    try {
      bool isAlreadyRegistered = await HttpService.isAlreadyRegistered(number);
      return isAlreadyRegistered;
    } catch (e) {
      return false;
    }
  }

  void _getOTP(number) async {
    print('get otp from login screen');

    setState(() {
      _isClicked = true;
    });
    try {
      bool registered = await _isAlreadyRegistered(number);
      print(registered);
      if (registered) {
        print("if registered");
        bool otp_sent = await HttpService.getOTP(number);
        if (otp_sent) {
          print("OTP sent");
          Navigator.pushReplacement(
              context,
              MaterialPageRoute(
                  builder: (BuildContext context) => VerifyOTP(
                        number: number,
                        type: 'login',
                      )));
        } else {
          print("Failed to send OTP.");
          _showSnackbar("Failed to send OTP.");
          setState(() {
            _isClicked = false;
          });
        }
      } else {
        _showSnackbar("You are not registered. Please sign up first.");
        setState(() {
          _isClicked = false;
        });
      }
    } catch (e) {
      print("Exception Error!!");
      _showSnackbar("Failed to send OTP.");
      setState(() {
        _isClicked = false;
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        key: _snackbarKey,
        resizeToAvoidBottomInset: false,
        body: VerifyNumber(_getOTP, _isClicked));
  }
}
