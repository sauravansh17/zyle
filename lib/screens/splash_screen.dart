import 'package:ZYLE/screens/landing_page.dart';
import 'package:ZYLE/services/app_storage/app_storage.dart';
import 'package:flutter/material.dart';
import 'login_signup_screen.dart';
import 'dart:async';
import 'package:flutter/services.dart';
import 'package:ZYLE/utils/screen_size.dart';

class SplashScreen extends StatefulWidget {
  @override
  _SplashState createState() => _SplashState();
}

class _SplashState extends State<SplashScreen> {
  bool isAccessTokenExist = false;
  int splashScreenDelay = 3;
  ScreenSize _ss;
  @override
  void initState() {
    super.initState();
    // _loadWidget();

    AppStorage()
        .isKeyExist('access_token')
        .then((value) => {
              if (value)
                {
                  setState(() {
                    isAccessTokenExist = true;
                  })
                }
            })
        .catchError((e) => SystemNavigator.pop());
    _loadWidget();
  }

  _loadWidget() async {
    var _duration = Duration(seconds: splashScreenDelay);
    return Timer(_duration, () => navigateAfterSplash(isAccessTokenExist));
  }

  void navigateAfterSplash(isAccessTokenExist) {
    if (isAccessTokenExist) {
      AppStorage().get('full_number').then((full_number) => {
            Navigator.pushReplacement(
                context,
                MaterialPageRoute(
                    builder: (BuildContext context) => LandingPage(full_number)))
          });

      // print("landing page");
    } else {
      Navigator.pushReplacement(
          context,
          MaterialPageRoute(
              builder: (BuildContext context) => LoginSignupScreen()));
      // print("loginsignup screen");
    }
  }

  @override
  Widget build(BuildContext context) {
    _ss = ScreenSize(context);

    return MaterialApp(
      title: 'Splash Screen',
      theme: ThemeData(primarySwatch: Colors.blue),
      home: Scaffold(
        backgroundColor: Colors.purple,
        body: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            // Text(
            //   "ZYLE",
            //   style: TextStyle(fontSize: 50),
            // ),
            Align(
              alignment: Alignment.center,
              child: Image(
                image: AssetImage('assets/images/logo.png'),
                height: _ss.sH(40),
                width: _ss.sW(40),
              ),
            )
          ],
        ),
      ),
    );
  }
}
