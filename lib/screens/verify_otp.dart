import 'dart:convert';
import 'package:ZYLE/screens/landing_page.dart';
import 'package:ZYLE/screens/signup/user_info_s1.dart';
import 'package:flutter/material.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/services.dart';
import 'package:pinput/pin_put/pin_put.dart';
import 'package:ZYLE/services/http_service/http_service.dart';
import 'package:ZYLE/utils/screen_size.dart';

class VerifyOTP extends StatefulWidget {
  final Map<String, dynamic> number;
  final String type;
  // bool isClicked = false;
  const VerifyOTP({Key key, @required this.number, @required this.type})
      : super(key: key);

  @override
  VerifyOTPState createState() => VerifyOTPState();
}

class VerifyOTPState extends State<VerifyOTP> {
  final TextEditingController _pinPutController = TextEditingController();
  final FocusNode _pinPutFocusNode = FocusNode();
  final GlobalKey<ScaffoldState> _snackbarKey = new GlobalKey<ScaffoldState>();
  bool _isClicked = false;
  ScreenSize _ss;
  BoxDecoration get _pinPutDecoration {
    return BoxDecoration(
      border: Border.all(color: Colors.deepPurpleAccent),
      borderRadius: BorderRadius.circular(15.0),
    );
  }

  void _showSnackbar(String message) {
    _snackbarKey.currentState.showSnackBar(new SnackBar(
      content: new Text(message),
    ));
  }

  void _verifyOTP(number, pin) async {
    // print(number);
    // print(pin);
    String val = jsonEncode(number);
    Map<String, dynamic> valMap = jsonDecode(val);
    String full_number = valMap.values.toList()[1] + valMap.values.toList()[0];

    setState(() {
      _isClicked = true;
    });
    try {
      bool isVerified = await HttpService.verifyOTP(number, pin);
      if (isVerified) {
        if (widget.type == 'login') {
          // AppStorage().set('full_number', full_number);
          Navigator.pushReplacement(
              context,
              MaterialPageRoute(
                  builder: (BuildContext context) => LandingPage(full_number)));
        } else {
          // signup
          Navigator.pushReplacement(
              context,
              MaterialPageRoute(
                  builder: (BuildContext context) => UserInfoS1(number)));
        }
      } else {
        _showSnackbar("Failed to verify OTP.");
        setState(() {
          _isClicked = false;
        });
      }
    } catch (e) {
      print("Exception error found");
      setState(() {
        _isClicked = false;
      });
    }
  }

  void _resendOTP(number) async {
    print(number);
    try {
      var isOTPSent = await HttpService.getOTP(number);
      if (isOTPSent) {
        print("OTP resent");
        _showSnackbar("OTP resent.");
      } else {
        print("Failed to resend OTP.");
        _showSnackbar("Failed to resend OTP.");
      }
    } catch (e) {
      print("Exception Error");
    }
  }

  @override
  Widget build(BuildContext context) {
    // print(widget.number);
    _ss = ScreenSize(context);

    return MaterialApp(
      title: "Verify OTP",
      home: Scaffold(
        key: _snackbarKey,
        resizeToAvoidBottomInset: false,
        backgroundColor: Color(0xffb29fff),
        body: SingleChildScrollView(
          child: Column(
            children: [
              Center(
                child: Container(
                  child: Image(
                    image: AssetImage('assets/images/cv.png'),
                    height: _ss.sH(30),
                    width: _ss.sW(50),
                  ),
                ),
              ),
              Container(
                  alignment: Alignment.centerLeft,
                  padding: EdgeInsets.only(left: _ss.sW(10)),
                  child: SizedBox(
                    width: _ss.sW(90),
                    child: Text(
                      "Enter the OTP",
                      style: TextStyle(
                          fontSize: _ss.sH(4),
                          color: Color(0xff555555),
                          fontWeight: FontWeight.w700),
                    ),
                  )),
              Container(
                alignment: Alignment.centerLeft,
                padding: EdgeInsets.only(left: _ss.sW(10)),
                child: SizedBox(
                  width: 273,
                  child: Text(
                    "We protect our community by making sure everyone on VC is real.",
                    style: TextStyle(
                      color: Color(0xff4e4e4e),
                      fontSize: _ss.sH(2.5),
                      fontFamily: "Montserrat",
                      fontWeight: FontWeight.w400,
                    ),
                  ),
                ),
              ),
              Container(
                alignment: Alignment.center,
                padding: EdgeInsets.only(
                    left: _ss.sW(10), top: _ss.sH(7), right: _ss.sW(10)),
                child: PinPut(
                  fieldsCount: 4,
                  // onSubmit: (String pin) => _showSnackBar(pin, context),
                  onSubmit: (String pin) => {print(pin)},
                  inputFormatters: [
                    new FilteringTextInputFormatter.allow(RegExp("[0-9]"))
                  ],
                  focusNode: _pinPutFocusNode,
                  controller: _pinPutController,
                  submittedFieldDecoration: _pinPutDecoration.copyWith(
                      borderRadius: BorderRadius.circular(20.0),
                      color: Color(0xfff3f3f3)),
                  selectedFieldDecoration: _pinPutDecoration,
                  followingFieldDecoration: _pinPutDecoration.copyWith(
                    borderRadius: BorderRadius.circular(5.0),
                    border: Border.all(
                      color: Colors.deepPurpleAccent.withOpacity(.5),
                    ),
                  ),
                ),
              ),
              Container(
                width: _ss.sW(50),
                height: _ss.sH(7),
                margin: EdgeInsets.only(top: _ss.sH(15)),
                child: RaisedButton(
                  onPressed: _isClicked
                      ? null
                      : () {
                          print("verify otp");
                          print(_pinPutController.text);
                          if (_pinPutController.text.length < 4 ||
                              _pinPutController.text == '') {
                            _showSnackbar("Enter 4-digit pin");
                          } else {
                            _verifyOTP(widget.number, _pinPutController.text);
                          }
                        },
                  // padding: const EdgeInsets.all(0.0),
                  textColor: Colors.white,
                  elevation: 5.0,
                  color: Color(0xff7e23b6),
                  padding: EdgeInsets.all(0.0),

                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(20)),
                  child: Container(
                    // width: double.infinity,
                    padding: const EdgeInsets.all(10.0),
                    // alignment: Alignment.center,
                    child: Text("Verify OTP"),
                  ),
                ),
              ),
              Container(
                alignment: Alignment.center,
                margin: EdgeInsets.only(top: 20.0),
                child: RichText(
                    text: TextSpan(
                        text: "Resend OTP",
                        recognizer: TapGestureRecognizer()
                          ..onTap = () => {_resendOTP(widget.number)})),
              )
            ],
          ),
        ),
      ),
    );
  }
}
