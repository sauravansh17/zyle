import 'package:ZYLE/services/app_storage/app_storage.dart';
import 'package:ZYLE/services/http_service/http_service.dart';
import 'package:ZYLE/widgets/custom_floating_btn.dart';
import 'package:ZYLE/screens/user/chat_list.dart';
import 'package:flutter/material.dart';

class HomeScreen extends StatefulWidget {
  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  bool user_verified = false;
  @override
  void initState() {
    print('home init state');
    authUser();
    super.initState();
  }

  void authUser() async {
    try {
      String token = await AppStorage().get('access_token');
      bool isAuthenticated = await HttpService.authUser(token);
      if (isAuthenticated) {
        print("user verified");
        setState(() {
          user_verified = true;
        });
      } else {
        print('failed to verify user');
      }
    } catch (e) {
      print('failed to verify user');
    }
  }

  @override
  Widget build(BuildContext context) {
    if (!user_verified) {
      return Scaffold(
        body: Center(
          child: Text("Verifying..."),
        ),
      );
    }
    return MaterialApp(
      title: 'Home Screen',
      theme: ThemeData(primaryColor: Color(0xffb29fff)),
      home: Scaffold(
        body: DefaultTabController(
          length: 4,
          child: Scaffold(
            appBar: AppBar(
              backgroundColor: Color(0xffb29fff),
              bottom: TabBar(
                indicatorColor: Colors.white,
                tabs: [
                  Tab(
                    text: "Chat",
                  ),
                  Tab(
                    text: "Status",
                  ),
                  Tab(
                    text: "Match",
                  ),
                  Tab(
                    text: "Request",
                  ),
                ],
              ),
              title: Center(child: Text('Home')),
            ),
            body: TabBarView(
              children: [
                ChatList(),
                Center(
                  child: Text("Status"),
                ),
                Center(
                  child: Text("Match"),
                ),
                Center(
                  child: Text("Request"),
                )
              ],
            ),
          ),
        ),
        floatingActionButton: CustomFloatingBtn(),
      ),
    );
  }
}
