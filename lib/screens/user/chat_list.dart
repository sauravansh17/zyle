import 'package:ZYLE/services/app_storage/app_storage.dart';
import 'package:ZYLE/services/firebase/firebase_service.dart';
import 'package:ZYLE/services/http_service/http_service.dart';
import 'package:ZYLE/utils/screen_size.dart';
import 'package:flutter/material.dart';
import 'package:ZYLE/models/message_model.dart';
import 'package:ZYLE/screens/chat_room.dart';
import 'package:ZYLE/models/user_model.dart';

class ChatList extends StatefulWidget {
  @override
  _ChatListState createState() => _ChatListState();
}

class _ChatListState extends State<ChatList> {
  final receiverId = "a18751fd-e33a-4899-9cf9-b7bad358b104";
  List<User> senders;
  List<Message> messages;
  String senderId;
  bool user_verified = false;
  ScreenSize _ss;
  @override
  void initState() {
    print('chatlist init state');
    authUser();
    super.initState();
  }

  void authUser() async {
    try {
      String token = await AppStorage().get('access_token');
      String senderId = await AppStorage().get('user_id');
      bool isAuthenticated = await HttpService.authUser(token);
      if (isAuthenticated) {
        print("user verified");

        setState(() {
          user_verified = true;
          senderId = senderId;
        });
      } else {
        print('failed to verify user');
      }
    } catch (e) {
      print('failed to verify user');
    }
  }

  @override
  Widget build(BuildContext context) {
    // print(senderId);
    _ss = ScreenSize(context);

    
    if (!user_verified) {
      return Scaffold(
        body: Center(
          child: Text("Retrieving chats..."),
        ),
      );
    }
    return StreamBuilder(
        // stream: null,
        // stream: FirebaseService.getChatlist(senderId),

        builder: (context, snapshot) {
          // if (snapshot.data == null) {
          //   return Center(child: Text("No chats found."));
          // }

          return ListView.builder(
            // itemCount: chats.length,
            itemCount: 3,
            // itemCount: snapshot.data.documents.length,

            itemBuilder: (BuildContext context, int index) {
              // print(snapshot.data.documents[index]['sendeId']);
              //  List<Message> chats = [
              // Message(
              //   sender: ironMan,
              //   time: '5:30 PM',
              //   text: 'Hey dude! Even dead I\'m the hero. Love you 3000 guys.',
              //   unread: true,
              // ),
              // final Message chat = chats[index];
              return Column(
                children: [
                  Center(
                    child: Text('id1'),
                  )
                ],
              );
              // return GestureDetector(
              //   onTap: () => Navigator.push(
              //     context,
              //     MaterialPageRoute(
              //       builder: (_) => ChatRoom(
              //         user: chat.sender,
              //       ),
              //     ),
              //   ),
              //   child: Container(
              //     padding: EdgeInsets.symmetric(
              //       horizontal: 20,
              //       vertical: 15,
              //     ),
              //     child: Row(
              //       children: <Widget>[
              //         Container(
              //           padding: EdgeInsets.all(2),
              //           decoration: chat.unread
              //               ? BoxDecoration(
              //                   borderRadius:
              //                       BorderRadius.all(Radius.circular(40)),
              //                   border: Border.all(
              //                     width: 2,
              //                     color: Theme.of(context).primaryColor,
              //                   ),
              //                   boxShadow: [
              //                     BoxShadow(
              //                       color: Colors.grey.withOpacity(0.5),
              //                       spreadRadius: 2,
              //                       blurRadius: 5,
              //                     ),
              //                   ],
              //                 )
              //               : BoxDecoration(
              //                   shape: BoxShape.circle,
              //                   boxShadow: [
              //                     BoxShadow(
              //                       color: Colors.grey.withOpacity(0.5),
              //                       spreadRadius: 2,
              //                       blurRadius: 5,
              //                     ),
              //                   ],
              //                 ),
              //           child: CircleAvatar(
              //             radius: 35,
              //             backgroundImage: AssetImage(chat.sender.imageUrl),
              //           ),
              //         ),
              //         Container(
              //           width: _ss.sW(65),
              //           padding: EdgeInsets.only(
              //             left: _ss.sW(4),
              //           ),
              //           child: Column(
              //             children: <Widget>[
              //               Row(
              //                 mainAxisAlignment: MainAxisAlignment.spaceBetween,
              //                 children: <Widget>[
              //                   Row(
              //                     children: <Widget>[
              //                       Text(
              //                         chat.sender.name,
              //                         style: TextStyle(
              //                           fontSize: _ss.sH(2),
              //                           fontWeight: FontWeight.bold,
              //                         ),
              //                       ),
              //                       chat.sender.isOnline
              //                           ? Container(
              //                               margin:
              //                                   const EdgeInsets.only(left: 5),
              //                               width: 7,
              //                               height: 7,
              //                               decoration: BoxDecoration(
              //                                 shape: BoxShape.circle,
              //                                 color: Theme.of(context)
              //                                     .primaryColor,
              //                               ),
              //                             )
              //                           : Container(
              //                               child: null,
              //                             ),
              //                     ],
              //                   ),
              //                   Text(
              //                     chat.time,
              //                     style: TextStyle(
              //                       fontSize: _ss.sH(1.5),
              //                       fontWeight: FontWeight.w300,
              //                       color: Colors.black54,
              //                     ),
              //                   ),
              //                 ],
              //               ),
              //               SizedBox(
              //                 height: _ss.sH(2),
              //               ),
              //               Container(
              //                 alignment: Alignment.topLeft,
              //                 child: Text(
              //                   chat.text,
              //                   style: TextStyle(
              //                     fontSize: _ss.sH(1.7),
              //                     color: Colors.black54,
              //                   ),
              //                   overflow: TextOverflow.ellipsis,
              //                   maxLines: 2,
              //                 ),
              //               ),
              //             ],
              //           ),
              //         ),
              //       ],
              //     ),
              //   ),
              // );
            },
          );
        });
  }
}
