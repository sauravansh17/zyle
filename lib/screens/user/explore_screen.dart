import 'package:ZYLE/services/app_storage/app_storage.dart';
import 'package:ZYLE/services/http_service/http_service.dart';
import 'package:flutter/material.dart';

class ExploreScreen extends StatefulWidget {
  @override
  _ExploreScreenState createState() => _ExploreScreenState();
}

class _ExploreScreenState extends State<ExploreScreen> {
  bool user_verified = false;
  @override
  void initState() {
    print('explore init state');
    authUser();
    super.initState();
  }

  void authUser() async {
    try {
      String token = await AppStorage().get('access_token');
      bool isAuthenticated = await HttpService.authUser(token);
      if (isAuthenticated) {
        print("user verified");
        setState(() {
          user_verified = true;
        });
      } else {
        print('failed to verify user');
      }
    } catch (e) {
      print('failed to verify user');
    }
  }

  @override
  Widget build(BuildContext context) {
      if (!user_verified) {
      return Scaffold(
        body: Center(
          child: Text("Verifying..."),
        ),
      );
    }
    return Scaffold(
      body: Center(
        child: Text("Explore screen"),
      ),
    );
  }
}
