import 'package:ZYLE/screens/login_signup_screen.dart';
import 'package:ZYLE/services/app_storage/app_storage.dart';
import 'package:ZYLE/services/http_service/http_service.dart';
import 'package:flutter/material.dart';

class UserProfile extends StatefulWidget {
  @override
  _UserProfileState createState() => _UserProfileState();
}

class _UserProfileState extends State<UserProfile> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: ListView(
          children: [
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Container(
                  margin: EdgeInsets.only(left: 30.0),
                  child: Icon(
                    Icons.account_circle,
                    size: 100,
                  ),
                ),
                Container(
                  margin: EdgeInsets.only(right: 50.0),
                  child: Column(
                    children: [Text("Sarvottam kumar"), Text("Followers")],
                  ),
                )
              ],
            ),
            Column(
              children: [
                Container(
                  child: Center(
                    child: Text("About"),
                  ),
                ),
                Container(
                  child: Center(
                    child: Text("Photos"),
                  ),
                ),
                RaisedButton(
                    child: Center(
                      child: Text("Log out"),
                    ),
                    onPressed: () {
                      // HttpService.logoutUser(access_token)
                      AppStorage().logout();

                      Navigator.pushAndRemoveUntil(
                          context,
                          MaterialPageRoute(
                              builder: (BuildContext context) =>
                                  LoginSignupScreen()),
                          (Route<dynamic> route) => false);
                    })
              ],
            )
          ],
        ),
      ),
    );
  }
}
