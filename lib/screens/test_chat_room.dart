import 'package:ZYLE/services/firebase/firebase_service.dart';
import 'package:flutter/material.dart';
import 'package:connectivity/connectivity.dart';

class TestChatRoom extends StatefulWidget {
  @override
  _TestChatRoomState createState() => _TestChatRoomState();
}

class _TestChatRoomState extends State<TestChatRoom> {
  final senderId = "7a31c0b1-bf34-4830-970a-9ac335ad22cc";
  final receiverId = "a18751fd-e33a-4899-9cf9-b7bad358b104";

  void sendMsg(senderId, receiverId, msg) async {
    var isStored = await FirebaseService.storeMsg(senderId, receiverId, msg);
    var isSent = await FirebaseService.sendMsg(senderId, receiverId, msg);
    if (isStored && isSent) {
      print("msg sent");
    } else {
      print("faiiled to send msg");
    }
  }

  @override
  Widget build(BuildContext context) {
    return StreamBuilder(
        // stream: FirebaseService.getMsglist(senderId),
        builder: (BuildContext context, AsyncSnapshot snapshot) {
      if (snapshot.data == null) {
        return Center(
          child: RaisedButton(
            onPressed: () {
              // sendMsg(senderId, receiverId, "hello from sarvottam");
              print("btn click");
            },
            child: Text("no data foudn"),
          ),
        );
      }

      return ListView.builder(
          padding: EdgeInsets.all(10.0),
          // itemCount: snapshot.data.documents.length,
          // itemCount: snapshot.data.length,
          itemCount: 2,
          itemBuilder: (context, index) {
            print("chat item");

            return Center(
              child: Text("chat received"),
            );
          });
    });
  }
}
