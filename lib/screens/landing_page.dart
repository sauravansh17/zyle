import 'dart:convert';

import 'package:ZYLE/screens/login_signup_screen.dart';
import 'package:ZYLE/screens/user/crush_screen.dart';
import 'package:ZYLE/screens/user/explore_screen.dart';
import 'package:ZYLE/screens/user/home_screen.dart';
import 'package:ZYLE/screens/user/profile_screen.dart';
import 'package:ZYLE/services/app_storage/app_storage.dart';
import 'package:ZYLE/services/http_service/http_service.dart';
import 'package:flutter/material.dart';
import 'package:ZYLE/utils/screen_size.dart';
import 'package:firebase_core/firebase_core.dart';

class LandingPage extends StatefulWidget {
  final String full_number;
  LandingPage(this.full_number);
  @override
  _LandingPageState createState() => _LandingPageState();
}

class _LandingPageState extends State<LandingPage> {
  bool _firebase_initialized = false;
  bool _firebase_error = false;
  ScreenSize _ss;
  bool user_verified = false;
  int _selectedBottomIndex = 0;
  static const TextStyle optionStyle =
      TextStyle(fontSize: 30, fontWeight: FontWeight.bold);
  static List<Widget> _widgetOptions = <Widget>[
    new HomeScreen(),
    new ExploreScreen(),
    new CrushScreen(),
    new UserProfile(),
  ];

  void initializeFlutterFire() async {
    try {
      await Firebase.initializeApp();
      setState(() {
        _firebase_initialized = true;
      });
    } catch (e) {
      setState(() {
        _firebase_error = true;
      });
    }
  }

  @override
  void initState() {
    print(widget.full_number);
    print("landing page init state");
    userAuth();
    initializeFlutterFire();
    super.initState();
  }

  void userAuth() async {
    try {
      bool value = await AppStorage().isKeyExist('access_token');
      if (value) {
        // verify token
        try {
          String token = await AppStorage().get('access_token');
          bool isAuthenticated = await HttpService.authUser(token);
          if (isAuthenticated) {
            print("user verified");
            setState(() {
              user_verified = true;
            });
          } else {
            print('failed to verify user');
          }
        } catch (e) {
          print('failed to verify user');
        }
      } else {
        // get token
        try {
          Map<String, dynamic> out =
              await HttpService.getUserAuthToken(widget.full_number);

          // print(out['access_token']);
          if (out['access_token'] == null) {
            print("failed to get access token");
          } else {
            print("access token received");
            AppStorage().set('access_token', out['access_token']);
            AppStorage().set('user_id', out['user_id']);
            setState(() {
              user_verified = true;
            });
          }
        } catch (e) {
          print("failed to get access token");
        }
      }
    } catch (e) {
      print('failed to get access token from local storage');
    }
  }

  Future<void> _showAuthFailDialog() async {
    return showDialog<void>(
      context: context,
      barrierDismissible: false, // user must tap button!
      builder: (BuildContext context) {
        return AlertDialog(
          title: Text('Failed to verify user'),
          content: SingleChildScrollView(
            child: ListBody(
              children: <Widget>[
                Text('You need to login.'),
              ],
            ),
          ),
          actions: <Widget>[
            TextButton(
              child: Text('OK'),
              onPressed: () {
                Navigator.pushAndRemoveUntil(
                    context,
                    MaterialPageRoute(
                        builder: (context) => LoginSignupScreen()),
                    (Route<dynamic> route) => false);
              },
            ),
          ],
        );
      },
    );
  }

  void _onItemTapped(int index) {
    setState(() {
      _selectedBottomIndex = index;
    });
  }

  @override
  Widget build(BuildContext context) {
    _ss = ScreenSize(context);
    // AppStorage().isKeyExist('access_token').then((value) => print(value));
    // AppStorage().logout();
    // AppStorage().get('user_id').then((value) => print(value));
    if (!user_verified) {
      return Scaffold(
        body: Center(
          child: Text("Verifying..."),
        ),
      );
    }
    return Scaffold(
      body: Center(
        child: _widgetOptions.elementAt(_selectedBottomIndex),
      ),
      bottomNavigationBar: BottomNavigationBar(
        backgroundColor: Color(0xffb29fff),
        type: BottomNavigationBarType.fixed, // This is all you need!
        items: const <BottomNavigationBarItem>[
          BottomNavigationBarItem(
            icon: Icon(Icons.home),
            label: 'Home',
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.explore),
            label: 'Explore',
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.favorite),
            label: 'Crush',
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.person),
            label: 'Profile',
          ),
        ],
        currentIndex: _selectedBottomIndex,
        selectedItemColor: Colors.white,
        onTap: _onItemTapped,
      ),
    );
  }
}
