import 'package:ZYLE/screens/splash_screen.dart';
import 'package:ZYLE/screens/test_screen.dart';
// import 'package:ZYLE/screens/user/home_screen.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

void main() {
  WidgetsFlutterBinding.ensureInitialized();
  SystemChrome.setPreferredOrientations(
      [DeviceOrientation.portraitDown, DeviceOrientation.portraitUp]).then((_) {
    runApp(MaterialApp(
      // initialRoute: '/',
      // routes: {
      //   '/': (context) => SplashScreen(),
      //   '/home': (context) => HomeScreen(),
      // },
      debugShowCheckedModeBanner: false,
      title: 'Zyle',
      // home: SplashScreen()
      home: TestScreen(),
    ));
  });
}
